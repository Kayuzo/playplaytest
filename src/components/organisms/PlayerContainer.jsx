import React, {useEffect, useRef, useState} from 'react'
import styles from './PlayerContainer.module.css'
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import InputColor from 'react-input-color';
import PlayPlayer from '../../libs/playplayer/PlayPlayer.js'
import * as PIXI from 'pixi.js';

export default function PlayerContainer() {
  const container = useRef()
  const player = useRef()
  const [settings, setSettings] = useState({
    scenes : [
      {
        name : "TestScene",
        params : {
          seed : "ppseed",
          mainColor : 0x09417A,
          secondColor : 0x5fc8b4,
          text : "Creative Developer\nAt Playplay",
          textColor : 0xffffff,
          shadowTextColor : 0x41849C,
          svgUrl : "images/line.svg",
          stayVisible : true
        }
      }
    ]
  })
  const [needUpdate, setNeedUpdate] = useState(false);
  var curSettings = Object.assign({}, settings);

  useEffect(()=>{
    player.current = new PlayPlayer(container.current, settings)
    player.current.Play();
    return () => {
      if(player.current)
        player.current.Destroy();
    }
  },[settings])

  return (
      <div className={styles.application}>
        <center>
          <div className={styles.player} ref={container}>
          {needUpdate && 
            <div className={styles.needUpdateScreen}>
              <div className={styles.background}/>
              <Button className={styles.button} variant="contained" onClick={() => { setSettings(curSettings); setNeedUpdate(false);}}>Reload</Button>
            </div>
          }
          </div>
          
          <div className={styles.settings}>
            <h2>Settings</h2>
            <div className={styles.blockinputs}>
              <h4>Text</h4>
              <TextField id="outlined-multiline-static" label="Your Text" multiline rows={4} defaultValue={settings.scenes[0].params.text} variant="outlined" onChange={(event)=>{
                curSettings.scenes[0].params.text = event.target.value;
                if(!needUpdate) setNeedUpdate(true);
                }} inputProps={{
                  maxLength: 30,
                }}/>
              <h6>Text Color</h6>
              <InputColor initialValue={PIXI.utils.hex2string(settings.scenes[0].params.textColor)} className={styles.inputs} onChange={(color)=>{
                var hex = PIXI.utils.string2hex(color.hex.substring(0,7));
                if(hex !== curSettings.scenes[0].params.textColor)
                  setNeedUpdate(true);
                curSettings.scenes[0].params.textColor = hex;
                }} />
              <h6>Shadow Text Color</h6>
              <InputColor initialValue={PIXI.utils.hex2string(settings.scenes[0].params.shadowTextColor)} className={styles.inputs} onChange={(color)=>{
                var hex = PIXI.utils.string2hex(color.hex.substring(0,7));
                if(hex !== curSettings.scenes[0].params.shadowTextColor)
                  setNeedUpdate(true);
                curSettings.scenes[0].params.shadowTextColor = hex;
                }} />
            </div>
            <div className={styles.blockinputs}>
            <h4>Background</h4>
            <h6>Main Color</h6>
              <InputColor initialValue={PIXI.utils.hex2string(settings.scenes[0].params.mainColor)} className={styles.inputs} onChange={(color)=>{
                var hex = PIXI.utils.string2hex(color.hex.substring(0,7));
                if(hex !== curSettings.scenes[0].params.mainColor)
                  setNeedUpdate(true);
                curSettings.scenes[0].params.mainColor = hex;
                }} />
              <h6>Second Color</h6>
              <InputColor initialValue={PIXI.utils.hex2string(settings.scenes[0].params.secondColor)} className={styles.inputs} onChange={(color)=>{
                var hex = PIXI.utils.string2hex(color.hex.substring(0,7));
                if(hex !== curSettings.scenes[0].params.secondColor)
                  setNeedUpdate(true);
                curSettings.scenes[0].params.secondColor = hex;
                }} />
              <h6>Sprite Model</h6>
              <Select labelId="label" id="select" value={settings.scenes[0].params.svgUrl}onChange={(event)=>{
                curSettings.scenes[0].params.svgUrl = event.target.value;
                setNeedUpdate(true);
                }}>
                <MenuItem value="images/line.svg">Line</MenuItem>
                <MenuItem value="images/circle.svg">Circle</MenuItem>
              </Select>
            </div>
          </div>
        </center>
      </div>
  )
}