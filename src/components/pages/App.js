import './App.css';
import PlayerContainer from '../organisms/PlayerContainer'

function App() {
  return (
    <div className="App">
      <PlayerContainer></PlayerContainer>
    </div>
  );
}

export default App;
