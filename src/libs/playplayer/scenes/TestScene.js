import AnimScene from '../core/AnimScene.js';
import * as PIXI from 'pixi.js';
import SVGBackgroundPlayer from '../players/SVGBackgroundPlayer.js'
import AppearTextPlayer from '../players/AppearTextPlayer.js';
import WavyTextPlayer from '../players/WavyTextPlayer.js';

//Scene composed of a SVGBackgroundPlayer, a split text which appear in fade in with a wave animation
export default class TestScene extends AnimScene {
    constructor(app, params, onfinish) {
        super(app, params, onfinish);
        this.name = "TestScene"; 
    }

    _createInstance(params) {  
        super._createInstance(params); 
        //text : Text to display in the scene
        //textColor : Color of this text
        //shadowTextColor : Color of the text shadow
        //svgUrl : url of the sgv used to create the background
        this.text = (params.text)?params.text : "Hello World !";
        this.textColor = (params.textColor)?params.textColor : 0xffffff;
        this.shadowTextColor = (params.shadowTextColor)?params.shadowTextColor : 0x000000;
        this.svgUrl = (params.svgUrl)?params.svgUrl : "images/line.svg";
    }

    Prepare() {
        //Create the background player
        var bgPlayer = new SVGBackgroundPlayer(this.app, {
            seed : this.seed,
            offset : 0,
            loop : 8,
            duration : 1000,
            mainColor : this.mainColor,
            secondColor : this.secondColor,
            svgUrl : this.svgUrl,
            wrapMode : "REPEAT",
            svgSize : {width: 32, height: 32},
            offsetPosition : 0.15,
            offsetAngle : 25,
            repeatTiled : {x: 5, y: 5}
        })
        bgPlayer.Prepare();
        this.players.push(bgPlayer);
    
        //Create the shadow text player (Add AppearText and WavyText animation)
        var shadowTextPlayer = new AppearTextPlayer(this.app, {
            seed : this.seed,
            offset : 500,
            loop : false,
            duration : 250,
            maxSubAppear : 2,
            text : this.text,
            textStyle : new PIXI.TextStyle({
                fontFamily: 'Arial',
                fontSize: 36,
                fontWeight: 'bold',
                fill: this.shadowTextColor,
                lineJoin: 'round',
                align : 'center'
            }),
            anchor : {x : 0.5, y : 0.5},
            position : {x : (this.app.screen.width / 2) + 2, y : (this.app.screen.height / 2) + 2}
        })
        shadowTextPlayer.Prepare();
        shadowTextPlayer.AddAnim(new WavyTextPlayer(this.app, {
            offset : 750,
            loop : 2,
            duration : 1000,
            directionWave : {x: 1, y: 2}
        }, () => {
            //To call onfinish callback when this animation is finished. For this scene, it is this player which is the last to play
            this._onFinish();
        }));
        this.players.push(shadowTextPlayer);
    
        //Create the main text player (Add AppearText and WavyText animation)
        var mainTextPlayer = new AppearTextPlayer(this.app, {
            seed : this.seed,
            offset : 750,
            loop : false,
            duration : 250,
            maxSubAppear : 2,
            text : this.text,
            textStyle : new PIXI.TextStyle({
                fontFamily: 'Arial',
                fontSize: 36,
                fontWeight: 'bold',
                fill: this.textColor,
                lineJoin: 'round',
                align : 'center'
            }),
            anchor : {x : 0.5, y : 0.5},
            position : {x : (this.app.screen.width / 2), y : (this.app.screen.height / 2)}
        })
        mainTextPlayer.Prepare();
        mainTextPlayer.AddAnim(new WavyTextPlayer(this.app, {
            offset : 1000,
            loop : 2,
            duration : 1000,
            directionWave : {x: 2, y: 4}
        }));
        this.players.push(mainTextPlayer);
    }
}