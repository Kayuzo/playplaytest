import SplitTextPlayer from './SplitTextPlayer.js';
import anime from 'animejs';
import gen from 'random-seed';

//Player to animate a text. Letter by letter will be appear with a fade in.
export default class AppearTextPlayer extends SplitTextPlayer {
    constructor(app, params, onfinish) {
        super(app, params, onfinish);
        this.name = "AppearText";
    }

    _createInstance(params) {
        super._createInstance(params);
        //maxSubAppear : Max Number of letter which appear at the same time
        //rand : a static rand object generate by the seed value. Used here to choose how many letters (at the same time) and which ones will be appear in the order.
        this.maxSubAppear = (params.maxSubAppear)?params.maxSubAppear : 0;

        this.rand = gen.create(this.seed);
    }

    _prepareAnime() {
        //if the maxSubAppear is equal 0, all text will appear with a fade in
        this.maxSubAppear = Math.min(this.maxSubAppear, this.graphics.length);
        if(this.maxTwinAppear === 0) {
            this.graphics.forEach(element => {
                this.anims.push(anime({
                    targets: element,
                    alpha : [0, 1],
                    duration: this.duration,
                    easing: 'linear',
                    loop : this.loop,
                    delay : this.offset,
                    autoplay : false
                }));
            })  
        }
        else {           
            var subNumber = 0;
            var subSequence = [];
            //Create a random  int array. Each value determinate how many letter will appear at the same time in the array order
            while(subNumber < this.graphics.length) {
                var subValue = this.rand.intBetween(1, this.maxSubAppear);
                subNumber += subValue;
                //To be sure to not exceed the number of letter
                subSequence.push(subValue - Math.max(subNumber - this.graphics.length, 0 ));             
            }
            subNumber = 0;
            var subOffset = 0;
            var subDuration = this.duration / subSequence.length;
            var graphicsPool = this.graphics.slice();
            //Choose random letter, and use the subSequence, to determinate which letters will appear at the same time
            subSequence.forEach(sub => {
                for(var i = subNumber; i < subNumber + sub; ++i) {
                    var idRand = this.rand(graphicsPool.length);
                    var graphic = graphicsPool[idRand];
                    graphicsPool.splice(idRand, 1);
                    this.anims.push(anime({
                        targets: graphic,
                        alpha : [0, 1],
                        duration: subDuration,
                        easing: 'linear',
                        delay : this.offset + subOffset,
                        autoplay : false
                    }));
                }
                subNumber += sub;
                subOffset += subDuration;
            });
        } 
        
        //Add callback onfinish when all animations was finished
        if(!!this.onfinish)
            this.anims[this.anims.length - 1].finished.then(this.onfinish);
    }
}