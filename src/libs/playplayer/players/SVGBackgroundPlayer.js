import anime from 'animejs';
import { Sprite } from 'pixi.js';
import BackgroundPlayer from './BackgroundPlayer.js';
import gen from 'random-seed';

//Player to display some animated svg element
//The animation is a simple rotation
//You can define the wrap mode of this svg element : Clamp or Repeat
export default class SVGBackgroundPlayer extends BackgroundPlayer {
    constructor(app, params, onfinish) {
        super(app, params, onfinish);
        this.name = "SGVBackground";
    }
    _createInstance(params) {
        super._createInstance(params);
        //svgUrl : the url/relative path of the svg element displayed
        //wrapMode : Clamp -> Display once the svg element / Repeat -> Repeat the svg element on the whole scene 
        //svgSize : Svg element size
        //offsetPosition : value used to add some "noise" on the svg element position in Repeat Wrap mode, to avoid a simple grid position
        //offsetAngle : value used to animated the rotation of the svg element. The svg element will rotate from the initial rotation angle to this origin plus this offset.
        //repeatTiled : the column and row number of the repeat grid
        //rand : a static rand object generate by the seed value. Used to add a random noisy position in the repeat grid and set a initial rotation angle
        //graphicsAnimated : Only animated graphics in this array (here svg elements) 
        this.svgUrl = (params.svgUrl)?params.svgUrl : "";
        this.wrapMode = (params.wrapMode)?params.wrapMode : "CLAMP";
        this.svgSize = (params.svgSize)?params.svgSize : {width: 32, height: 32};

        this.offsetPosition = (params.offsetPosition)?params.offsetPosition : 0.15;
        this.offsetAngle = (params.offsetAngle)?params.offsetAngle : 15;

        this.repeatTiled = (params.repeatTiled)?params.repeatTiled : {x: 5, y: 5};

        this.rand = gen.create(this.seed);

        this.graphicsAnimated = [];
    }

    //To get the size of one tile
    get tiledSize() {
        return {width: (this.app.screen.width / this.repeatTiled.x), height : (this.app.screen.height / this.repeatTiled.y)};    
    }

    //To create a SVG in a specific position
    _prepareSVG(x, y) {
        var sprite = Sprite.from(this.svgUrl);
        sprite.anchor.set(0.5, 0.5);
        var startedRotation = this.rand(360);
        sprite.angle = startedRotation;
        sprite.width = this.svgSize.width;
        sprite.height = this.svgSize.height;
        sprite.tint = this.secondColor;
        sprite.visible = false;
        this.app.stage.addChild(sprite);
        this.graphics.push(sprite);
        this.graphicsAnimated.push(sprite);

        sprite.x = x;
        sprite.y = y;
    }

    _prepareGraphics() {
        super._prepareGraphics();

        if(this.svgUrl === "")
            return;

        //If Clamp mode, only create one SVG element in the scene origin
        if(this.wrapMode === "CLAMP") {
            this._prepareSVG(0,0);
        }
        //Else create a repeat grid of SVG element
        else if(this.wrapMode === "REPEAT") {
            for(var i = 0; i <= this.repeatTiled.x; ++i) {
                for(var j = 0; j <= this.repeatTiled.y; ++j) {
                    var noisyRadian = this.rand(360) / 180 * Math.PI;
                    var noisyPosition = {x : Math.cos(noisyRadian) * this.offsetPosition * this.tiledSize.width, y: Math.sin(noisyRadian) * this.offsetPosition * this.tiledSize.height};
                    this._prepareSVG(this.tiledSize.width * i + noisyPosition.x, this.tiledSize.height * j + noisyPosition.y)
                }       
            }
        }
    }

    _prepareAnime() {
        //Add rotation animation on all svg element
        this.anims.push(anime({
            targets: this.graphicsAnimated,
            angle :{
                value: '+=' + this.offsetAngle.toString(),
                duration: this.duration
            },
            duration: this.duration,
            easing: 'steps(2)',
            loop : this.loop,
            direction: 'alternate',
            delay : this.offset,
            autoplay : false
        }));

        //Add callback onfinish when all animations was finished
        if(!!this.onfinish)
            this.anims[this.anims.length - 1].finished.then(this.onfinish);
    }
}