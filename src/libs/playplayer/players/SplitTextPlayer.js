import { Text, Container } from 'pixi.js';
import TextPlayer from './TextPlayer.js';

//Player to split a text by character and display these letters
//Derived this player if you want animate letter by letter
export default class SplitTextPlayer extends TextPlayer {
    constructor(app, params, onfinish) {
        super(app, params, onfinish);
        this.name = "SplitText";
    }

    _createInstance(params) {
        super._createInstance(params);
    }

    _prepareGraphics() {
        //Create a main container to contain all text lines
        const container = new Container();
        this.app.stage.addChild(container);

        var letters = this.text.split('');
        var offsetX = 0; 
        //Set the line height by the fontsize
        var offsetY = this.textStyle.fontSize;
        var alignOffset = (this.textStyle.align === "left")? 0 :  (this.textStyle.align === "right")? 1 : 0.5;
        var linenumber = 0;

        //Create a line container to contain all letters of one line
        var lineContainer = new Container();
        container.addChild(lineContainer);

        //Foreach all letters to add letters in each line container
        for(var i = 0; i < letters.length; ++i) { 
            var element = letters[i];
            if(element === '\n') {
                lineContainer = new Container();
                container.addChild(lineContainer);
                linenumber++;
                offsetX = 0;
                lineContainer.y = linenumber * offsetY ;
                continue;
            }
            
            var letter = new Text(element, this.textStyle);
            letter.x = offsetX;
            letter.y =  0;

            lineContainer.addChild(letter);
            this.graphics.push(letter);

            //Increment the x position of the next letter by the width of the previous letter
            offsetX += letter.getBounds().width;
        }

        //Foreach each letter to adjust position according to the align value (left/right/center)
        container.children.forEach(element => {
            element.x += (container.width - element.width) * alignOffset;         
        });

        container.x = this.position.x;
        container.y = this.position.y;

        container.pivot.x = container.width * this.anchor.x;
        container.pivot.y = container.height * this.anchor.y;
    }
}