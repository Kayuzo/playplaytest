import SplitTextPlayer from './SplitTextPlayer.js';
import anime from 'animejs';

//Player to add a wave animation to split text
export default class WavyTextPlayer extends SplitTextPlayer {
    constructor(app, params, onfinish) {
        super(app, params, onfinish);
        this.name = "AppearText";
    }

    _createInstance(params) {
        super._createInstance(params);
        //directionWave : the direction of the wave animation
        //staggerTime : value in millisecond to stagger all letter animations
        this.directionWave = (params.directionWave)?params.directionWave : {x: 2, y: 2};
        this.staggerTime = (params.staggerTime)?params.staggerTime : 100;
    }

    _prepareAnime() {
        this.anims.push(this._applyAnime(this.graphics));
    }

    _applyAnime(graphics) {
        var anim = anime({
            targets: graphics,
            x :[
                { value: '+=' + this.directionWave.x.toString() },
                { value: '-=' + this.directionWave.x.toString() }
              ],
            y : [
                { value: '+=' + this.directionWave.y.toString() },
                { value: '-=' + this.directionWave.y.toString() }
              ],
            easing: 'steps(2)',
            delay : anime.stagger(this.staggerTime, {start: this.offset}),
            loop : this.loop,
            direction : 'alternate',
            autoplay : false
        })

        //Add callback onfinish when all animations was finished
        if(this.onfinish !== undefined)
            anim.finished.then(this.onfinish);
        return anim;       
    }
}