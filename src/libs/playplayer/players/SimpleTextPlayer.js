import { Text } from 'pixi.js';
import TextPlayer from './TextPlayer.js';

//Player to display a static text
export default class SimpleTextPlayer extends TextPlayer {
    constructor(app, params, onfinish) {
        super(app, params, onfinish);
        this.name = "SimpleText";
    }

    _createInstance(params) {
        super._createInstance(params);
    }

    _prepareGraphics() {
        var mainText = new Text(this.text, this.textStyle);
        mainText.anchor.set(this.anchor.x, this.anchor.y);
        mainText.x = this.position.x;
        mainText.y =  this.position.y;
        mainText.visible = false;
        this.app.stage.addChild(mainText);
        this.graphics.push(mainText);
    }
}