import { Graphics } from 'pixi.js';
import AnimPlayer from '../core/AnimPlayer.js';

//Player to add background to your scene
export default class BackgroundPlayer extends AnimPlayer {
    constructor(app, params, onfinish) {
        super(app, params, onfinish);
        this.name = "Background";
    }

    _createInstance(params) {
        super._createInstance(params);
    }

    _prepareGraphics() {
        super._prepareGraphics();
        var rect = new Graphics();
        rect.visible = false;
        rect.beginFill(this.mainColor);   
        rect.drawRect(0, 0, this.app.screen.width, this.app.screen.height);
        this.app.stage.addChild(rect);
        this.graphics.push(rect);
    }
}