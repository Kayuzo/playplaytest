import { TextStyle } from 'pixi.js';
import AnimPlayer from '../core/AnimPlayer.js';

//Abstract class to define a TextPlayer
//Derived of this class to create Player based on a text
export default class TextPlayer extends AnimPlayer {
    constructor(app, params, onfinish) {
        super(app, params, onfinish);
        if (this.constructor === TextPlayer) {
            throw new TypeError('Abstract class "TextPlayer" cannot be instantiated directly');
        }
        this.name = "Text";
    }

    _createInstance(params) {
        super._createInstance(params);
        this.text = (params.text)?params.text : "Hello World !";
        this.textStyle = (params.textStyle)?params.textStyle : new TextStyle({
            fontFamily: 'Arial',
            fontSize: 36,
            fontWeight: 'bold',
            fill: ['#ffffff'],
            lineJoin: 'round',
            align : 'center'
        });
    }
}