//Abstract class to define a player
//A player is one of the animated components of your scene.
export default class AnimPlayer {
    //app : Pixi application
    //params : parameters to describe your player
    //onfinish : define this callback to call it when this player is finished
    constructor(app, params, onfinish) {
        if (this.constructor === AnimPlayer) {
            throw new TypeError('Abstract class "AnimPlayer" cannot be instantiated directly');
        }
        this.name = "AnimPlayer";
        this.app = app; 
        this.graphics = [];
        this.anims = [];
        this.onfinish = onfinish;

        if (!params) params = {};
        this._createInstance(params);   
    }

    //Private function to set all properties with parameters
    _createInstance(params) {
        //seed : Seed use to manipulate random values
        //offset : offset time value to define the time after which the player will play
        //loop : If player is looping
        //duration : Duration of player animation
        //mainColor : Player main color
        //secondColor : Player second color
        this.seed = (params.seed)?params.seed : "ppseed";
        this.offset = (params.offset)?params.offset : 0;
        this.loop = (params.loop !== undefined)?params.loop : false;
        this.duration = (params.duration)?params.duration : 1000;
        this.mainColor = (params.mainColor)?params.mainColor : 0x000000;
        this.secondColor = (params.secondColor)?params.secondColor : 0xFFFFFF;

        //Player transform parameters
        this.anchor = (params.anchor)?params.anchor : {x : 0, y : 0};
        this.position = (params.position)?params.position : {x : 0, y : 0};
        this.rotation = (params.rotation)?params.rotation : 0;
        this.scale = (params.scale)?params.scale : {x : 1, y : 1};
        this.pivot = (params.pivot)?params.pivot : {x : 0, y : 0};
    }

    //Called to create different graphic elements
    _prepareGraphics() {}
    //Called to create different anime objects
    _prepareAnime() {}
    //Called to apply player anime to a grahics array
    _applyAnime(graphics) { return null; }
    //To prepare the player before to play
    Prepare() {
        this._prepareGraphics();
        this._prepareAnime();
    }

    //Play the player
    Play() {
        this.Display(true);

        this.anims.forEach(function(item, index, array) {
            item.play();
        });
    }

    //Pause the player
    Pause() {
        this.anims.forEach(function(item, index, array) {
            item.pause();
        });
    }

    //Stop the player (at the moment, only pause the player)
    Stop() {
        this.anims.forEach(function(item, index, array) {
            item.pause();
        });
    }

    Display(value)
    {
        this.graphics.forEach(function(item, index, array) {
            item.visible = value;
        });
    }

    //To add different extern animes player to this player
    //Used it if you want to merge another player with this player
    AddAnim(animePlayer) {
        if(!animePlayer || !(animePlayer._prepareAnime))
            return;

        var anims = animePlayer._applyAnime(this.graphics);
        if(!anims)
            return;

        if(anims instanceof Array)
            this.anims = this.anims.concat(anims);
        else
            this.anims.push(anims);
    } 
}