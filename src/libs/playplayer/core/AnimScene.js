//Abstract Class to define a scene
//A scene is composed of different player
//Derived of this class, to define your scene and add different players
export default class AnimScene {
    constructor(app, params, onfinish) {
        if (this.constructor === AnimScene) {
            throw new TypeError('Abstract class "AnimScene" cannot be instantiated directly');
        }
        this.name = "AnimScene";
        this.app = app;
        this.players = [];

        if (!params) params = {};
        this._createInstance(params);
        
        this.onfinish = onfinish;
    }

    _createInstance(params) {
        //seed : Seed use to manipulate random values
        //mainColor : Player main color
        //secondColor : Player second color
        //stayVisible : To derminate if scene stay displayed when it is finished
        this.seed = (params.seed)?params.seed : "ppseed";
        this.mainColor = (params.mainColor)?params.mainColor : 0x000000;
        this.secondColor = (params.secondColor)?params.secondColor : 0x000000;
        this.stayVisible = (params.stayVisible)?params.stayVisible : false;
    }

    _onFinish()
    {
        if(this.onfinish !== undefined)
            this.onfinish();
        if(!this.stayVisible)
            this.Hide();
    }

    //Called to prepare scene. Need to define this function in all derived classes
    Prepare() {}

    //Play all players
    Play() {
        this.players.forEach(element => {
            element.Play();
        });
    }

    //Pause all players
    Pause() {
        this.players.forEach(element => {
            element.Pause();
        });
    }

    //Stop all players
    Stop() {
        this.players.forEach(element => {
            element.Stop();
        });
    }

    Hide() {
        this.players.forEach(element => {
            element.Display(false);
        });
    }
}