import * as PIXI from 'pixi.js';
import TestScene from './scenes/TestScene.js'

//Template to link a template scene name to a template scene object
//When you create a new template scene, add it in this array
const TEMPLATE = {
    "TestScene" : (app, params, onfinish) => { return new TestScene(app, params, onfinish);}
};

//Main class of PlayPlayer libs
export default class PlayPlayer {
    constructor(parent, playplayObj) {
        this.parent = parent; 
        this.playplayObj = playplayObj;
        this.scenes = [];
        this.currentScene = null;
        this.isPrepared = false;

        //Create Pixi application context
        this.app = new PIXI.Application({ 
            antialias: true,
            resizeTo : parent,
            resolution : window.devicePixelRatio
        });
        parent.appendChild(this.app.view);
        this.app.resize();
    
        //Prepare different scenes with the playplayObj
        this.Prepare();
    }

    //To create all scenes and prepare the PlayPlayer
    Prepare()
    {
        this.Stop();

        if(!this.playplayObj)
            return;

        for(var i = 0; i < this.playplayObj.scenes.length; ++i) {
            var sceneObj = this.playplayObj.scenes[i];
            var scene = TEMPLATE[sceneObj.name](this.app, sceneObj.params);
            scene.Prepare();
            this.scenes.push(scene);
        }
        for(var j = 0; j < this.scenes.length; ++j) {
            let scene = this.scenes[j];
            let nextScene = (j !== this.scenes.length - 1) ? this.scenes[j + 1] : null;
            scene.onfinish = (nextScene === null)?this._onFinish : () => { nextScene.Play(); };
        }
        this.isPrepared = true;
    }

    //To play if the PlayPlayer is ready
    //Play the first scene if no scene is playing
    Play()
    {
        if(!this.isPrepared || this.scenes.length === 0)
            return;

        if(!this.currentScene)
            this.currentScene = this.scenes[0];

        this.currentScene.Play();
    }

    //To stop all scenes
    Stop()
    {
        this.scenes.forEach(element => {
            element.Stop();
        })
    }

    _onFinish()
    {
        //Add what you want to do when the last player has finished playing
    }

    //Call to stop and destroy PlayPlayer (Pixi application, Scenes ...)
    Destroy() {
        this.Stop();
        this.scenes = [];
        this.isPrepared = false;
        this.playplayObj = null;
        this.app.destroy(true, {children : true, baseTexture : true, texture : true});
    }
}